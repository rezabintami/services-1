module coba

go 1.15

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.6.3 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/google/uuid v1.1.2 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/kataras/go-sessions v3.0.0+incompatible
	github.com/kataras/go-sessions/v3 v3.3.0 // indirect
	github.com/klauspost/compress v1.11.1 // indirect
	github.com/satori/go.uuid v1.2.1-0.20181028125025-b2ce2384e17b // indirect
	github.com/valyala/fasthttp v1.16.0 // indirect
	golang.org/x/crypto v0.0.0-20201012173705-84dcc777aaee
)
